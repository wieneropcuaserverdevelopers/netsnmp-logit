# netsnmp-logit

Test project, cross platform, for verifying performance of LogIt (as used in quasar servers, e.g. wiener-opcua-server) when working together with netsnmp library. Program makes 1000 LogIt calls & times them, then initialises SNMP (single call to *init_snmp*) then makes another 1000 LogIt calls and times them.
- **Expectation**: LogIt callstake same amount of time before and after init_snmp called.
- **Result**: TRUE on linux, **FALSE** on windows.

## cmake windows build (created empty peer directory for building, called netsnmp-build-debug - command below invoked from there)
```
// generate VS2017 solution (NB: I used a Mingw64 bit bash terminal - hence linux-ish looking paths. Is still windows, runs vis studio etc.
cmake ../netsnmp-logit -DCMAKE_BUILD_TYPE=Debug -DCMAKE_TOOLCHAIN_FILE=boost_custom_win_VS2017.cmake -DNETSNMP_INCLUDE_DIR=/d/3rdPartySoftware/net-snmp/5.7.3/src/include -DNETSNMP_EXT_LIB_DIR=/d/3rdPartySoftware/net-snmp/5.7.3/build-debug/lib -G "Visual Studio 15 2017 Win64"

// build
cmake --build $(pwd) --config Debug
```

## cmake linux build (same directory setup as windows build)
```
// generate g++ makefiles - note snmp is built-in lib for linux (for centos7 anyway), so need need to providepaths for includes or binaries
cmake ../netsnmp-logit -DCMAKE_BUILD_TYPE=Debug -DCMAKE_TOOLCHAIN_FILE=boost_custom_cc7.cmake

// build
cmake --build $(pwd) --config Debug
```

## execution (same both platforms)
```
// best to remove all log files & directories before
rm -rf *log (or rm *log powershell)

// execute, takes ~10s to complete
./netsnmp-logit
```

## logfile analysis
I used this inverse grep command to extract the interesting part from the logs (i.e. not interested in the 2000 individual LogIt calls from function *wienerTestLogIt*, only interested in the timing summaries)
```
grep -v "wienerTestLogIt" file_00000.log
```

## Results
**Windows** Note ~7x performance degredation in LogIt calls after *init_snmp* called
```
$ grep -v "wienerTestLogIt" ./Debug/file_00000.log
2020-09-30 16:09.52.552488 [main.cpp:19, INF] initializeWinsock initialized successfully
2020-09-30 16:09.54.448981 [TestUtils.cpp:23, INF, TEST_CMP]
@i [1000] logTime1 [936.031us] logTime2 [952.021us]
*****
2020-09-30 16:09.54.449968 [main.cpp:30, INF] main initializing netsnmp...
2020-09-30 16:09.54.767324 [main.cpp:32, INF] main initialized netsnmp
2020-09-30 16:10.08.233138 [TestUtils.cpp:23, INF, TEST_CMP]
@i [2000] logTime1 [6761.4us] logTime2 [6695.27us]
*****

```

**Linux** Note performance of LogIt calls same before and after *init_snmp* called
```
bash-4.2$ grep -v "wienerTestLogIt" file_00000.log 
2020-09-30 16:13.42.034361 [TestUtils.cpp:23, INF, TEST_CMP] 
@i [1000] logTime1 [53.499us] logTime2 [47.623us]
*****
2020-09-30 16:13.42.034524 [main.cpp:30, INF] main initializing netsnmp...
2020-09-30 16:13.42.055496 [main.cpp:32, INF] main initialized netsnmp
2020-09-30 16:13.42.151426 [TestUtils.cpp:23, INF, TEST_CMP] 
@i [2000] logTime1 [47.826us] logTime2 [45.942us]
*****

```



