#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <LogIt.h>
#include <LogLevels.h>
#include "TestUtils.h"

Log::LogComponentHandle TEST_CMP;

void initializeWinsock()
{
#ifdef _WIN32	
	WSADATA wsaData;
	const int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0)
	{
		LOG(Log::ERR) << __FUNCTION__<< " WSAStartup failed, result [" << iResult << "]. Aborting";
		std::exit(1);
	}
	LOG(Log::INF) << __FUNCTION__ << " initialized successfully";
#endif	
}

int main(int argc, char* argv[])
{
	Log::initializeLogging(Log::INF);
	TEST_CMP = Log::registerLoggingComponent("TEST_CMP", Log::INF);
	initializeWinsock();

	for (size_t i = 0000; i < 1000; wienerTestLogIt(++i)) {};
	LOG(Log::INF) << __FUNCTION__ << " initializing netsnmp...";
	init_snmp("netsnmp-logit");
	LOG(Log::INF) << __FUNCTION__ << " initialized netsnmp";
	for (size_t i = 1000; i < 2000; wienerTestLogIt(++i)) {};

	return 0;
}
