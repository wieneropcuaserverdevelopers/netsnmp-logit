#include "TestUtils.h"
#include <chrono>
#include <LogLevels.h>


void wienerTestLogIt(const size_t i)
{
	static size_t callCounter = 0;

	const auto p1 = std::chrono::high_resolution_clock::now();
	LOG(Log::INF, TEST_CMP) << __FUNCTION__ << "+";
	const auto p2 = std::chrono::high_resolution_clock::now();
	LOG(Log::INF, TEST_CMP) << __FUNCTION__ << "-";
	const auto p3 = std::chrono::high_resolution_clock::now();

	static size_t logTime1 = 0;
	static size_t logTime2 = 0;
	logTime1 += std::chrono::duration_cast<std::chrono::microseconds>(p2 - p1).count();
	logTime2 += std::chrono::duration_cast<std::chrono::microseconds>(p3 - p2).count();

	if (++callCounter % 1000 == 0)
	{
		LOG(Log::INF, TEST_CMP) << std::endl <<
			"********" << __FUNCTION__ << "******" << std::endl <<
			"@i [" << i << "] logTime1 [" << float(logTime1) / 1000 << "us] logTime2 [" << float(logTime2) / 1000 << "us]" << std::endl <<
			"*****";
		logTime1 = 0;
		logTime2 = 0;
	}
}
